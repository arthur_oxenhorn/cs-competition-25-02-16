package bots;
import java.util.Random;
import pirates.game.PirateBot;
import pirates.game.PirateGame;
import java.util.concurrent.atomic.AtomicInteger;

public class MyBot implements PirateBot
{

    static Random rnd = new Random();
    //an atomic class the holds an integer, should be used when accessing the same integer from different threads
    static AtomicInteger leaves = new AtomicInteger();
    static int finishedThreads = 0;
    //Number of total nodes in tree
    static final int nodes = 3;
    //Number of nodes per thread
    static final int batch = 1;
    //Depth of tree
    static final int depth = 3;

    @Override
    public void doTurn(PirateGame game) {

        double lastTime;
        lastTime = System.currentTimeMillis();
        //Number of threads = number of nodes divided by the nodes to process per thread
        Thread[] temp = new Thread[nodes/batch];
        //Create the classes to feed the threads
        threadedFunctionTree[] tempTree = new threadedFunctionTree[nodes/batch];

        for (int i = 0; i < temp.length; i++) {
            tempTree[i] = new threadedFunctionTree(batch, depth, 0, true, game);
            temp[i] = new Thread(tempTree[i]);
            temp[i].start();
        }
        //wait for the thread to finish //god damn it igor comments fucking comments
        for (int i = 0; i < temp.length; i++) {
            try {temp[i].join();}
            catch (Exception e) {}
        }
        for (threadedFunctionTree tr : tempTree) {
            game.debug(tr.getScore() + "");
        }
        //noby got time to wait if the bug happens once in 100 turns
        game.debug("The tree(function) is done  : " + (System.currentTimeMillis() - lastTime));
        game.debug("Finished threads: " + finishedThreads);
        game.debug("Done " + leaves + " leaves!");
        finishedThreads = 0;
        leaves = new AtomicInteger();
    }

    //recursive minimax function
    static double minimax(int nodes, int maxDepth, int depth, boolean myTurn) {
        if (depth >= maxDepth) {
            return doSomeMath();
        }
        //Best score for current node
        double bestScore;
        //Temporary score holder
        double currentScore;
        if (myTurn)
            bestScore = Double.MIN_VALUE;  //Maximizer
        else
            bestScore = Double.MAX_VALUE;  //Minimizer

        for (int i = 0; i < nodes; i++) {
            currentScore = minimax(nodes, maxDepth, depth + 1, !myTurn);
            //Maximizer
            if (myTurn && currentScore > bestScore)
                bestScore = currentScore;
            //Minimizer
            if (!myTurn && currentScore < bestScore)
                bestScore = currentScore;
        }
        return bestScore;
    }

    //an random math function for testing
    static int doSomeMath() {
        int random = Math.abs(rnd.nextInt()%100);
        leaves.incrementAndGet();
//        random = Math.pow(Math.E, random) * rnd.nextDouble();
        System.out.println(random);
        return random;
    }
}


class threadedFunctionTree implements Runnable {
    public int batch;
    public int maxDepth;
    public int depth;
    //True for my turn, false for enemy turn
    public boolean myTurn;
    PirateGame game;
    public double bestScore;

    public threadedFunctionTree(int batch, int maxDepth, int depth, boolean myTurn, PirateGame game ) {
        this.batch = batch;
        this.maxDepth = maxDepth;
        this.depth = depth;
        this.myTurn = myTurn;
        this.game = game;
    }

    @Override
    public void run() {
        if (myTurn)
            bestScore = Double.MIN_VALUE;
        else
            bestScore = Double.MAX_VALUE;
        double currentScore;
        for (int i = 0; i < MyBot.batch; i++) {
            currentScore = MyBot.minimax(MyBot.nodes, maxDepth-1, depth, myTurn);
            if (myTurn && currentScore > bestScore)
                bestScore = currentScore;   //Maximizer
            if (!myTurn && currentScore < bestScore)
                bestScore = currentScore;   //Minimizer
        }
        MyBot.finishedThreads++;
    }

    public double getScore() {
        return bestScore;
    }
}